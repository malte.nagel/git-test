
/**
 * Ein HelloWorld-Beispiel
 * 
 * @author Malte Nagel, Prof. Dr.-Ing. Emre Cakar
 */
public class Start
{

    /*
     * Die main-Methode. Gibt "Hallo Welt!" aus.
     */
    public static void main(String[] args)
    {
        System.out.println("Hallo Welt!");
        System.out.println("Dies ist ein Test.");
    }

}
